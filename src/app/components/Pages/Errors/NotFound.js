import * as m from "mithril";
import DetailError from "./DetailError";

export default {

    view() {
        return (
            <DetailError title="404: Not found">
                uh oh...
            </DetailError>
        );
    }
};
